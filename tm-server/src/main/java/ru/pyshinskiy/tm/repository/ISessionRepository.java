package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Session;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface ISessionRepository extends EntityRepository<Session, String> {

    @Query(value = "select s from Session s where s.user.id = :userId", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Session> findAllByUserId(@QueryParam("userId") String userId);

    @Query("delete from Session s")
    void removeAll() throws Exception;

    @Query(value = "select s from Session s where s.id = :id and s.user.id = :userId", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Session findOneByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete from Session s where s.user.id = :userId and s.id = :id")
    void removeByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete from Session s where s.user.id = :userId")
    void removeAllByUserId(@QueryParam("userId") @NotNull final String userId) throws Exception;
}
