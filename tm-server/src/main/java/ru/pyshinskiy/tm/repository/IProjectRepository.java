package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Project;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface IProjectRepository extends EntityRepository<Project, String> {

    @Query(value = "select p from Project p where p.user.id = :userId", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Project> findAllByUserId(@QueryParam("userId") String userId);

    @Query("delete from Project p")
    void removeAll() throws Exception;

    @Query(value = "select p from Project p where p.id = :id and p.user.id = :userId", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Project findOneByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query(value = "select p from Project p where p:user.id = :userId and p.name = :name", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Project> findByName(@QueryParam("userId") @NotNull final String userId, @QueryParam("name") @NotNull final String name);

    @Query(value = "select p from Project p where p:user.id = :userId and p.description = :description", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Project> findByDescription(@QueryParam("userId") @NotNull final String userId, @QueryParam("description") @NotNull final String description);

    @Query("delete from Project p where p.user.id = :userId and p.id = :id")
    void removeByUserId(@QueryParam("userId") @NotNull final String userId, @QueryParam("id") @NotNull final String id) throws Exception;

    @Query("delete from Project p where p.user.id = :userId")
    void removeAllByUserId(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select p from Project p where p.user.id= :userId order by p.createTime", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Project> sortByCreateTime(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select p from Project p where p.user.id= :userId order by p.startDate", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Project> sortByStartDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select p from Project p where p.user.id= :userId order by p.finishDate", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Project> sortByFinishDate(@QueryParam("userId") @NotNull final String userId) throws Exception;

    @Query(value = "select p from Project p where p.user.id= :userId order by p.status", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    List<Project> sortByStatus(@QueryParam("userId") @NotNull final String userId) throws Exception;
}
