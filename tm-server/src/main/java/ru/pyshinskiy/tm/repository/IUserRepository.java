package ru.pyshinskiy.tm.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.User;

import javax.persistence.QueryHint;

@Repository
public interface IUserRepository extends EntityRepository<User, String> {

    @Query("delete from User u")
    void removeAll() throws Exception;

    @Query(value = "select u from User u where u.login = :login", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    User getUserByLogin(@QueryParam("login") @NotNull final String login) throws Exception;
}
