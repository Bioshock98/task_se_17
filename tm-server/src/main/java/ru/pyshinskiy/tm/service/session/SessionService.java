package ru.pyshinskiy.tm.service.session;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.repository.ISessionRepository;
import ru.pyshinskiy.tm.service.AbstractService;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;

@Transactional
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Inject
    private ISessionRepository sessionRepository;

    @Override
    @Nullable
    public Session findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception{
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        @Nullable final Session session;
        try {
            session = sessionRepository.findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        return session;
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        sessionRepository.removeByUserId(userId, id);
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        @Nullable final Session session;
        try {
            session = sessionRepository.findBy(id);
        }
        catch (NoResultException e) {
            return null;
        }
        return session;
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        @NotNull final List<Session> sessions = sessionRepository.findAll();
        return sessions;
    }

    @Override
    @NotNull
    public List<Session> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions = sessionRepository.findAllByUserId(userId);
        return sessions;
    }

    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        sessionRepository.save(session);
    }

    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        sessionRepository.save(session);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        sessionRepository.remove(findOne(id));
    }

    @Override
    public void removeAll() throws Exception {
        sessionRepository.removeAll();
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("userId is empty or null");
        sessionRepository.removeAllByUserId(userId);
    }
}
