package ru.pyshinskiy.tm.service.locator;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.session.ISessionService;
import ru.pyshinskiy.tm.service.task.ITaskService;
import ru.pyshinskiy.tm.service.user.IUserService;

import javax.inject.Inject;

public class ServiceLocator implements IServiceLocator {

    @Inject
    private IProjectService projectService;

    @Inject
    private ITaskService taskService;

    @Inject
    private IUserService userService;

    @Inject
    private ISessionService sessionService;

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return sessionService;
    }
}
