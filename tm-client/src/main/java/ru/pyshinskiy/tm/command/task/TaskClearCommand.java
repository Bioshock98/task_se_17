package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

public final class TaskClearCommand extends AbstractCommand {

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_clear";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASK]");
        taskEndpoint.removeAllTasksByUserId(sessionService.getSessionDTO());
        System.out.println("[ALL TASKS REMOVED]");
    }
}
