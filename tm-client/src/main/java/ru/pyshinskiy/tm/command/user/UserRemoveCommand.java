package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUsers;

public final class UserRemoveCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;

    @Override
    public boolean isAllowed() {
        if(sessionService.getSessionDTO() == null) return false;
        return sessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove existing user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER ID");
        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(sessionService.getSessionDTO());
        printUsers(users);
        final int userNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String userId = users.get(userNumber).getId();
        userEndpoint.removeUser(sessionService.getSessionDTO(), userId);
        System.out.println("USER REMOVED");
    }
}
