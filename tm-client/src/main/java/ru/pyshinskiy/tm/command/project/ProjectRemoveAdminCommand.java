package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectRemoveAdminCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if(sessionService.getSessionDTO() == null) return false;
        return sessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_remove_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "remove any selected project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects(sessionService.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        projectEndpoint.removeProject(sessionService.getSessionDTO(), projectId);
        System.out.println("[PROJECT REMOVED]");
    }
}
