package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUser;

public final class UserViewCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_view";
    }

    @Override
    @NotNull
    public String description() {
        return "show user info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER VIEW");
        @NotNull final String userId = sessionService.getSessionDTO().getUserId();
        printUser(userEndpoint.findOneUser(sessionService.getSessionDTO(), userId));
        System.out.println("[OK]");
    }
}
